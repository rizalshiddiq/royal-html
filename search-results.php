<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home static-page">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/gold-coin.jpg">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Search Results</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Search "Gold"</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <h4 class="text-gold font-title mb-2">Results for "gold"</h4>
            <p class="card-text font-300">
                We found <strong>14 search results</strong> for keyword "<strong class="text-gold">gold</strong>"
            </p>

            <br>

            <div class="card mb-4">
                <div class="card-body">
                    <h3 class="text-gold font-title mb-2">Product</h3>
                    <div class="row mt-4">
                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Golden Metal</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Golden Metal</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Golden Metal</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Golden Metal</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-3 mb-4">
                            <div class="card card-product">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                </div>
                                <div class="card-body">
                                    <div class="content-list">
                                        <p class="card-text text-secondary">Metal</p>
                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                    </div>
                                    <hr>
                                    <p class="card-text text-default text-center mb-0">
                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                            Detail
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h3 class="text-gold font-title mb-2">News</h3>
                    <div class="row mt-4">
                        <div class="col-12 col-md-4 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">Event</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">Antam's 50 years of work</h5>
                                    <p class="card-text text-default">04 Juni 2019</p>
                                    <p class="card-text font-300">
                                        Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
                                    </p>
                                    <br>
                                    <a href="news-detail.php" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-4 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">Event</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">Antam's 50 years of work</h5>
                                    <p class="card-text text-default">04 Juni 2019</p>
                                    <p class="card-text font-300">
                                        Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
                                    </p>
                                    <br>
                                    <a href="news-detail.php" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-4 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">Event</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">Antam's 50 years of work</h5>
                                    <p class="card-text text-default">04 Juni 2019</p>
                                    <p class="card-text font-300">
                                        Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
                                    </p>
                                    <br>
                                    <a href="news-detail.php" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>