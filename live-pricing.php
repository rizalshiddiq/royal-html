<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/sub-header.png">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Live Pricing</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Live Pricing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row mb-2">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body anual-pricing">
                            <h6 class="font-desc text-gold font-500 mb-3">Buy XAU/IDR</h6>
                            <h3 class="font-desc text-default font-500 mb-0">
                                638, 693, 900.00
                                <span class="status-pricing text-primary"><i class="fa fa-caret-down" aria-hidden="true"></i> 18, 600.00</span>
                            </h3>

                            <hr>

                            <div class="d-flex flex-wrap justify-content-between">
                                <div>
                                    <span class="status-pricing font-desc">Sell USD/IDR</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">14,147.00</h5>
                                </div>

                                <div class="text-right">
                                    <span class="status-pricing font-desc">Bid XAU/USD</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">1,529.51</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body anual-pricing">
                            <h6 class="font-desc text-gold font-500 mb-3">Sell XAU/IDR</h6>
                            <h3 class="font-desc text-default font-500 mb-0">
                                634, 772, 700.00
                                <span class="status-pricing text-primary"><i class="fa fa-caret-down" aria-hidden="true"></i> 55, 400.00</span>
                            </h3>

                            <hr>

                            <div class="d-flex flex-wrap justify-content-between">
                                <div>
                                    <span class="status-pricing font-desc">Buy USD/IDR</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">14,147.00</h5>
                                </div>

                                <div class="text-right">
                                    <span class="status-pricing font-desc">Ask XAU/USD</span>
                                    <h5 class="font-desc text-red font-500 mt-2 mb-0">1,529.51</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body anual-pricing">
                    <h4 class="text-uppercase head-title mt-0 mb-2">XAU/IDR</h4>
                    <p class="head-sub-desc mb-2">Lorem ipsum dolor sit amet</p>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>