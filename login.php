<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="card">
                        <div class="card-body body-lg">
                            <h3 class="text-capitalize head-title mt-0 mb-2">Got an Account?</h3>
                            <p class="head-sub-desc">Login to your account</p>

                            <br>

                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="" id="" class="form-control"
                                    placeholder="example@mail.com" aria-describedby="helpId">
                            </div>

                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" name="" id="" class="form-control" placeholder="*******"
                                    aria-describedby="helpId">
                            </div>

                            <div class="d-flex align-items-center justify-content-between flex-wrap mb-3">
                                <div>
                                    <label class="customcheck m-t-10">Keep me signed in
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

                                <div>
                                    <p class="m-0">
                                        <a href="forgot-password.php" class="text-primary">Forgot Password?</a>
                                    </p>
                                </div>
                            </div>

                            <a href="#" class="btn btn-primary btn-lg w-100">Yes, Login Now</a>

                            <p class="mb-0 mt-4 text-center font-400">
                                Don’t have account? <a href="register.php" class="font-500 text-primary">Register
                                    Now</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>