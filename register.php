<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="card">
                        <div class="card-body body-lg">
                            <h3 class="text-capitalize head-title mt-0 mb-2">Create My Account</h3>
                            <p class="head-sub-desc">Complete the form below</p>

                            <br>
                            
                            <div class="form-group">
                                <label for="">Fullname</label>
                                <input type="text" name="" id="" class="form-control" placeholder="Enter your name" aria-describedby="helpId">
                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="" id="" class="form-control" placeholder="example@mail.com" aria-describedby="helpId">
                            </div>

                            <div class="form-group">
                                <label for="">Phone Number</label>
                                <input type="number" name="" id="" class="form-control" placeholder="08456456234" aria-describedby="helpId">
                            </div>

                            <div class="form-group">
                                <label for="">Date of Birth</label>
                                <input type="date" name="" id="" class="form-control" placeholder="10 Mei 1990" aria-describedby="helpId">
                            </div>

                            <div class="form-group">
                                <label for="">Address</label>
                                <textarea name="" class="form-control" placeholder="Enter address" id="" cols="30" rows="10"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" name="" id="" class="form-control" placeholder="*******" aria-describedby="helpId">
                            </div>

                            <div class="form-group">
                                <label for="">Re-type Password</label>
                                <input type="password" name="" id="" class="form-control" placeholder="*******" aria-describedby="helpId">
                            </div>

                            <div>
                                <label class="customcheck m-t-10">I accept Royal Raffles Capital <a href="#" class="text-primary font-500">Terms of Service</a>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div>
                                <label class="customcheck m-t-10">Subscribe Newsletter
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <br>

                            <a href="#" class="btn btn-primary btn-lg w-100">Create Account</a>
                            
                            <p class="mb-0 mt-4 text-center font-400">
                                Already have an account? <a href="login.php" class="font-500 text-primary">
                                    Login now
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>