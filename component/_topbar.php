<div class="top-notif d-none d-md-block">
    <div class="container">
        <div class="content-section">
            <div class="component">
                <p class="text-primary mb-0">
                    <i class="fa fa-phone fa-lg mr-1" aria-hidden="true"></i> (021) 29943424
                </p>
            </div>

            <div class="component">
                <p class="text-primary mb-0">
                    <i class="fa fa-envelope fa-lg mr-1" aria-hidden="true"></i> info@royalraffles.co
                </p>
            </div>

            <div class="component">
                <ul class="social-media">
                    <li>
                        <a href="#" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>

                    <li>
                        <a href="#" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>

                    <li>
                        <a href="#" class=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                    </li>

                    <li>
                        <a href="#" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>

                    <li>
                        <a href="#" class=""><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>