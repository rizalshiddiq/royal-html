<script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="vendor/popper/popper.min.js"></script>
<script type="text/javascript" src="vendor/slick/js/slick.min.js"></script>
<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<script type="text/javascript" src="vendor/easyzoom/easyzoom.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/add-easyzoom.js"></script>
<script type="text/javascript" src="assets/js/sidebar-mobile.js"></script>
<script type="text/javascript" src="assets/js/slick-add.js"></script>

<script>
    function initMap() {
    var myLatLng = {lat: -6.882332, lng: 107.619021};

    var map = new google.maps.Map(document.getElementById('map-contact'), {
        zoom: 20,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'sign doctor'
    });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBkW3l4xJIvrDcLskksqmaJ8g1oZZBKLQ&callback=initMap"></script>

</body>
</html>