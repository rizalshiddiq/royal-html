<!-- Footer -->
<footer>
    <div class="footer-section">
        <div class="container footer-left-xs">
            <div class="row justify-content-between">
                <div class="col-12 col-lg-4 mb-3">
                    <a class="logo" href="#">
                        <img src="assets/images/icons/logo-royal.png" alt="" class="img-fluid">
                    </a>

                    <p class="mt-4">
                        Lorem ipsum dolor sita met qonqueror dolor sita met qonqueror dolor sita
                    </p>
                </div>

                <div class="col-12 col-md-12 col-lg-8 mb-3">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-2">
                            <h4 class="title-list-footer mb-2">Information</h4>
                            <ul class="nav-footer">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about.php">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="our-product.php">Our Product</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="live-pricing.php">Live Pricing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="news.php">News</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact.php">Contact Us</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-2">
                            <h4 class="title-list-footer mb-2">Help Center</h4>
                            <ul class="nav-footer">
                                <li class="nav-item">
                                    <a class="nav-link" href="how-to-purchase.php">How To Purchase</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Payment Method</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Shipment</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Return</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-12 col-md-4 col-lg-4 col-xl-6 mb-2">
                            <h4 class="title-list-footer mb-2">Subscribe Newsletter</h4>
                            <p>Lorem ipsum dolor sita met qonqueror</p>
                            <div class="form-group">
                                <input type="text" class="form-control form-transparent"
                                    placeholder="Write your email">
                            </div>
                            <a href="#" class="btn btn-secondary w-100">Subscribe</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-section">
        <div class="container d-flex flex-wrap align-items-center justify-content-between">
            <p class="mt-2 mb-2">© Royal Raffles Capital, 2019 All Rights Reserved</p>
            <ul class="list mt-2 mb-2">
                <li>
                    <a href="#">
                        <p class="mb-0">Privacy Policy</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <p class="mb-0">Terms and Conditions</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>