<!-- Modal Show Search Bar -->
<div id="BlockSearchForm" class="off">
    <div id="search" class="open">
        <div class="container h-100">
            <button data-widget="remove" id="RemoveClass" class="close" type="button">
                <svg width="15" height="15" viewBox="0 0 16 16">
                    <path fill="#4B0000" fill-rule="evenodd"
                        d="M16 1.622L9.622 8 16 14.378 14.378 16 8 9.622 1.622 16 0 14.378 6.378 8 0 1.622 1.622 0 8 6.378 14.378 0z">
                    </path>
                </svg>
            </button>

            <div class="row h-100">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <form action="#" method="" autocomplete="off" class="form-search-section">
                        <h4 class="text-gold mb-3">Search Anything</h4>
                        <input type="text" placeholder="Type search keywords here..." value="" name="term"
                            id="term">
                        <div class="mt-4">
                            <button type="submit" class="btn btn-primary btn-lg w-100">
                                <i class="fa fa-search mr-1" aria-hidden="true"></i> Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>