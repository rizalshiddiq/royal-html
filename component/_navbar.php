<!-- Navbar Menu Desktop -->
<nav class="navbar navbar-expand-lg desktop-navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand pt-0" href="index.php">
                <img src="assets/images/icons/logo-royal.png" alt="" class="img-fluid logo-home">
            </a>
        </div>
        <div id="ShowMenuMobile" class="navbar-toggler">
            <i class="fa fa-bars text-gold" aria-hidden="true"></i>
        </div>

        <div class="collapse navbar-collapse" id="navbarDesktop">
            <ul class="navbar-nav mr-auto ml-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about.php">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="our-product.php">Our Product</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="live-pricing.php">Live Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="news.php">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact-us.php">Contact Us</a>
                </li>
            </ul>
            <ul class="navbar-nav nav-line">
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Login</a>
                </li>
                <li class="nav-item pr-0">
                    <a class="nav-link search-icon-btn" href="javascript:void(0)"><i class="fa fa-search"
                            aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Sidebar Menu Mobile -->
<div id="mySidenav" class="sidenav">
    <div class="container-fluid mr-1 ml-1">
        <div class="closebtn text-gold" id="CloseMenuMobile">
            <svg width="15" height="15" viewBox="0 0 16 16">
                <path fill="#FFF" fill-rule="evenodd"
                    d="M16 1.622L9.622 8 16 14.378 14.378 16 8 9.622 1.622 16 0 14.378 6.378 8 0 1.622 1.622 0 8 6.378 14.378 0z">
                </path>
            </svg>
        </div>

        <div class="mt-3 mb-3">
            <img src="assets/images/icons/logo-royal.png" width="80" class="img-fluid" alt="">
        </div>

        <ul class="navbar-nav-mobile m-t-40">
            <li class="active">
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="about.php">About Us</a>
            </li>
            <li>
                <a href="our-product.php">Our Product</a>
            </li>
            <li>
                <a href="live-pricing.php">Live Pricing</a>
            </li>
            <li>
                <a href="news.php">News</a>
            </li>
            <li>
                <a href="contact-us.php">Contact Us</a>
            </li>
        </ul>

        <hr>

        <div class="d-flex-center">
            <p class="mb-0"><a href="login.php" class="text-gold text-uppercase">Login</a></p>
            <div class="text-white mr-3 ml-3">|</div>
            <a class="search-icon-btn" href="javascript:void(0)">
                <p class="mb-0"><i class="fa fa-search text-gold" aria-hidden="true"></i></p>
            </a>
        </div>
    </div>

    <div class="foot-contact">
        <p class="text-primary mb-1">
            <i class="fa fa-phone mr-1" aria-hidden="true"></i> (021) 29943424
        </p>

        <p class="text-primary mb-1">
            <i class="fa fa-envelope mr-1" aria-hidden="true"></i> info@royalraffles.co
        </p>

        <ul class="social-media mt-2">
            <li>
                <a href="#" class=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </li>

            <li>
                <a href="#" class=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </li>

            <li>
                <a href="#" class=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            </li>

            <li>
                <a href="#" class=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </li>

            <li>
                <a href="#" class=""><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
            </li>
        </ul>
    </div>
</div>