<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="Demo project">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Royal Raffles Capital</title>
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/font-awesome.min.css">
	<!-- Slick -->
	<link id="effect" rel="stylesheet" type="text/css" media="all" href="vendor/slick/css/slick.css" />
	<link rel="stylesheet" type="text/css" media="all" href="vendor/slick/css/slick-theme.css" />
	<!-- Main Style -->
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
	<style>
		#ProductFilterSection{ order: 1; }
		#ProductContentSection{ order: 2; }
		#ProductPaginationSection{ order: 3; }
	</style>
</head>
<body>