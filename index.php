<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
	<header id="headerSection" class="home-section">
		<div id="demo" class="carousel slide" data-ride="carousel" data-interval="3000">
			<span class="d-none d-md-block">
				<ul class="carousel-indicators">
					<li data-target="#demo" data-slide-to="0" class="active"></li>
					<li data-target="#demo" data-slide-to="1"></li>
					<li data-target="#demo" data-slide-to="2"></li>
				</ul>
			</span>

			<div class="carousel-inner">
				<div class="carousel-item active">
					<div class="dark-img">
						<img class="d-block w-100" src="assets/images/pictures/header.png" alt="first slide">
					</div>
					<div class="carousel-caption container d-flex align-items-center">
						<div class="text-block">
							<h5>Bespoke Engagement Rings Couple</h5>
							<p>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia voluptatum ad
								magnam dolorem maiores cumque nihil
								maxime ratione, quaerat, accusamus
							</p>
							<a href="#" class="btn btn-primary btn-lg">View Our Collection</a>
						</div>
					</div>
				</div>

				<div class="carousel-item">
					<div class="dark-img">
						<img class="d-block w-100" src="assets/images/pictures/header.png" alt="second slide">
					</div>
					<div class="carousel-caption container d-flex align-items-center">
						<div class="text-block">
							<h5>Bespoke Engagement Rings Couple</h5>
							<p>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia voluptatum ad
								magnam dolorem maiores cumque nihil
								maxime ratione, quaerat, accusamus
							</p>
							<a href="#" class="btn btn-primary btn-lg">View Our Collection</a>
						</div>
					</div>
				</div>

				<div class="carousel-item">
					<div class="dark-img">
						<img class="d-block w-100" src="assets/images/pictures/header.png" alt="third slide">
					</div>
					<div class="carousel-caption container d-flex align-items-center">
						<div class="text-block">
							<h5>Bespoke Engagement Rings Couple</h5>
							<p>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia voluptatum ad
								magnam dolorem maiores cumque nihil
								maxime ratione, quaerat, accusamus
							</p>
							<a href="#" class="btn btn-primary btn-lg">View Our Collection</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<section class="content-blank pb-0">
		<div class="container">
			<h3 class="text-capitalize head-title">Why Royal Raffles Capital?</h3>
			<p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror</p>

			<div class="row m-t-70">
				<div class="col-12 col-sm-6 col-md-4 mb-4">
					<div class="card-body text-center">
						<div class="card-center-absolute">
							<img src="assets/images/icons/gold-pyramid.png" width="75"
								class="img-fluid icon-featured mb-4" alt="">
							<div class="card-title mb-3">Gold 99.99%</div>
							<p class="font-300 mb-0">
								We guarantee the authenticity of the product and the purity of 99.99%
							</p>
						</div>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-md-4 mb-4">
					<div class="card-body text-center">
						<div class="card-center-absolute">
							<img src="assets/images/icons/safe-box.png" width="75"
								class="img-fluid icon-featured mb-4" alt="">
							<div class="card-title mb-3">Safe Box Service</div>
							<p class="font-300 mb-0">
								The smart way to invest gold without risk is lost, with the purchase price of gold
								that is cheaper
							</p>
						</div>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-md-4 mb-4">
					<div class="card-body text-center">
						<div class="card-center-absolute">
							<img src="assets/images/icons/credit-card.png" width="75"
								class="img-fluid icon-featured mb-4" alt="">
							<div class="card-title mb-3">Secure Transaction</div>
							<p class="font-300 mb-0">
								Website security is equipped with SSL Certificate encryption.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="content-blank">
		<div class="container mb-4">
			<h3 class="text-capitalize head-title">Best Product</h3>
			<p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror</p>

			<nav class="m-t-40 mb-2">
				<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
					<a class="nav-item text-uppercase active" data-toggle="tab"
						href="#navTabsIndonesiaBatik">Indonesian batik series</a>
					<a class="nav-item text-uppercase" data-toggle="tab" href="#navTabsPureSilver">pure silver
						99.5%</a>
					<a class="nav-item text-uppercase" data-toggle="tab" href="#navTabsDinarDirham">dinar and dirham
						coins</a>
					<a class="nav-item text-uppercase" data-toggle="tab" href="#navTabsIndustrialProduct">industrial
						products</a>
					<a class="nav-item text-uppercase" data-toggle="tab" href="#navTabsGiftSeries">Gift Series</a>
					<a class="nav-item text-uppercase" data-toggle="tab" href="#navTabsClassicProduct">Classic
						Product</a>
				</div>
			</nav>

			<div class="tab-content py-3" id="nav-tabContent">
				<div class="tab-pane fade active show" id="navTabsIndonesiaBatik">
					<div class="slick_best-product_1 slick-custom">
						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/kalung-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/cincin-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/cincin-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="navTabsPureSilver">
					<div class="slick_best-product_2 slick-custom">
						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/kalung-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/cincin-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>

						<div class="card card-slick-custom">
							<div class="block-over-img img-lg">
								<img class="card-img-top img-fluid w-100" src="assets/images/pictures/cincin-1.png">
							</div>
							<div class="card-body">
								<p class="card-text text-secondary">Metal</p>
								<p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
								<p class="card-text text-gold font-300">Rp 2.000.000</p>
								<hr>
								<p class="card-text text-default text-center">
									<a href="product-detail.php" class="text-default">
										<i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
										Detail
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="tab-pane fade" id="navTabsDinarDirham">Dinar Dirham</div>
				<div class="tab-pane fade" id="navTabsIndustrialProduct">Industrial Product</div>
				<div class="tab-pane fade" id="navTabsGiftSeries">Gift Series</div>
				<div class="tab-pane fade" id="navTabsClassicProduct">Classic Product</div>
			</div>
		</div>
	</section>

	<section class="content-blank gray-section">
		<div class="container mb-4">
			<h3 class="text-capitalize head-title">News</h3>
			<p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror</p>

			<div class="slick_home-news slick-custom m-t-40">
				<div class="card card-slick-custom">
					<div class="block-over-img img-lg">
						<img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
					</div>
					<div class="card-body">
						<h6 class="text-gold font-title mb-2">Antam's 50 years of work</h6>
						<p class="card-text text-default">04 Juni 2019</p>
						<p class="card-text font-300">
							Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
						</p>
						<br>
						<a href="news-detail.php" class="btn btn-primary">Read More</a>
					</div>
				</div>

				<div class="card card-slick-custom">
					<div class="block-over-img img-lg">
						<img class="card-img-top img-fluid w-100" src="assets/images/pictures/complete-edition.png">
					</div>
					<div class="card-body">
						<h6 class="text-gold font-title mb-2">Complete Edition Series II</h6>
						<p class="card-text text-default">04 Juni 2019</p>
						<p class="card-text font-300">
							Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
						</p>
						<br>
						<a href="news-detail.php" class="btn btn-primary">Read More</a>
					</div>
				</div>

				<div class="card card-slick-custom">
					<div class="block-over-img img-lg">
						<img class="card-img-top img-fluid w-100" src="assets/images/pictures/antam's-design.png">
					</div>
					<div class="card-body">
						<h6 class="text-gold font-title mb-2">Antam's Innovation Design</h6>
						<p class="card-text text-default">04 Juni 2019</p>
						<p class="card-text font-300">
							Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
						</p>
						<br>
						<a href="news-detail.php" class="btn btn-primary">Read More</a>
					</div>
				</div>

				<div class="card card-slick-custom">
					<div class="block-over-img img-lg">
						<img class="card-img-top img-fluid w-100" src="assets/images/pictures/antam's-design.png">
					</div>
					<div class="card-body">
						<h6 class="text-gold font-title mb-2">Antam's Innovation Design</h6>
						<p class="card-text text-default">04 Juni 2019</p>
						<p class="card-text font-300">
							Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
						</p>
						<br>
						<a href="news-detail.php" class="btn btn-primary">Read More</a>
					</div>
				</div>
			</div>

			<div class="text-center m-t-100">
				<a href="news.php" class="btn btn-primary btn-lg">Show More News</a>
			</div>
		</div>
	</section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>