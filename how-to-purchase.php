<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home static-page">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/gold-coin.jpg">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">How to Purchase</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">How to Purchase</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <h3 class="text-gold font-title mb-2">How To Purchase</h3>
                    <hr>
                    <p class="card-text font-300">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                    </p>

                    <p class="card-text font-300">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                    </p>

                    <p class="card-text font-300">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id sed obcaecati aliquid alias necessitatibus laudantium animi maxime aut quibusdam harum sint cumque perspiciatis aliquam tempore blanditiis facilis culpa, voluptatibus nihil.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>