$(document).ready(function () {
    $(window).scroll(function () {
        if ($(document).scrollTop() > 100) {
            $(".desktop-navigation").addClass("nav-top");
        } else {
            $(".desktop-navigation").removeClass("nav-top");
        }
    });

    $(".search-icon-btn").click(function () {
        $('#BlockSearchForm').addClass('popup-box-on');
    });

    $("#RemoveClass").click(function () {
        $('#BlockSearchForm').removeClass('popup-box-on');
    });
});