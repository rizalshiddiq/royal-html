<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/sub-header.png">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">About Us</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">About Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="header-feature-about">
                        <img src="assets/images/pictures/about-img-1.png" class="img-fluid" alt="">
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-4">
                    <div class="about-text-block">
                        <h2>Providing Best For You Since 1998</h2>
                        <br>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat.
                            Duis aute irure dolor in reprehenderit in voluptate velit .
                        </p>
                        <p>
                            Some of our customer say’s that they trust us and buy our product without any hagitation
                            because they belive us and
                            always happy to buy our product.
                        </p>
                        <p>
                            We provide the beshat they trusted us and buy our product without any hagitation because
                            they belive us and always
                            happy to buy.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <h3 class="text-capitalize head-title">Our Vision & Mission</h3>
            <p class="head-sub-desc">Visit our shop to see amazing creations from our designers</p>

            <div class="row m-t-70">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="row">
                                <div class="col-12 col-sm-2">
                                    <img src="assets/images/icons/target.png" width="75"
                                        class="img-fluid icon-featured mb-4" alt="">
                                </div>

                                <div class="col-12 col-sm-10">
                                    <div class="card-title text-default mb-2">Vision</div>
                                    <p class="font-300 mb-0">
                                        We aim to offer a final product that’s indistinguishable from the one you
                                        dreamed
                                        up. Watch your box concept take shape
                                        in real time and get exactly what you envisioned. The sky’s the limit!.
                                        Packwire
                                        offers nothing short
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="row">
                                <div class="col-12 col-sm-2">
                                    <img src="assets/images/icons/startup.png" width="75"
                                        class="img-fluid icon-featured mb-4" alt="">
                                </div>
                                <div class="col-12 col-sm-10">
                                    <div class="card-title text-default mb-2 mb-3">Mission</div>
                                    <p class="font-300 mb-0">
                                        We see how much work you’ve put into your package design so when it comes to
                                        execution, Packwire offers nothing short of
                                        the best. Your ideal custom box comes to life with leading printing tools
                                        and
                                        the
                                        highest quality materials.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="img-about-foot">
        <img src="assets/images/pictures/about-img-2.png" class="img-fluid" alt="">
    </div>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>