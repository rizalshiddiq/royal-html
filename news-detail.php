<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/sub-header.png">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">News</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="news.php">News</a></li>
                            <li><a href="#">Antam's 50 years of work</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="row mr-auto">
            <div class="col-12 col-lg-8 mb-4">
                <div class="card" style="border-left: 0 !important;">
                    <div class="block-over-img img-xxl">
                        <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                    </div>
                    <div class="read-article mb-1">
                        <h1 class="title-article">Antam's 50 years of work</h1>
                        <p class="date-post">04 Juni 2019</p>
                        <br>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eius debitis quidem ratione, eum atque. Fuga architecto, a, sequi ad dolorem nulla facere cumque, dolore quasi molestias blanditiis dolores. Modi?
                        </p>

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eius debitis quidem ratione, eum atque. Fuga architecto, a, sequi ad dolorem nulla facere cumque, dolore quasi molestias blanditiis dolores. Modi?
                        </p>

                        <blockquote>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis voluptatibus, vel recusandae consequuntur non sed laudantium repellendus aliquid cupiditate fugiat, labore optio asperiores cum porro quos expedita? Impedit, minus sit!
                        </blockquote>

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eius debitis quidem ratione, eum atque. Fuga architecto, a, sequi ad dolorem nulla facere cumque, dolore quasi molestias blanditiis dolores. Modi?
                        </p>

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem eius debitis quidem ratione, eum atque. Fuga architecto, a, sequi ad dolorem nulla facere cumque, dolore quasi molestias blanditiis dolores. Modi?
                        </p>
                    </div>
                    
                    <hr>
                    
                    <div class="read-article">
                        <h5 class="text-capitalize head-title mt-0 mb-3">Share Article</h5>
                        <div class="mb-4">
                            <a href="#" class="btn btn-fb mr-2">
                                <i class="fa fa-facebook-square fa-lg mr-2" aria-hidden="true"></i> Facebook
                            </a>

                            <a href="#" class="btn btn-twitter mr-2">
                                <i class="fa fa-twitter fa-lg mr-2" aria-hidden="true"></i> Twitter
                            </a>

                            <a href="#" class="btn btn-google-plus mr-2">
                                <i class="fa fa-google-plus fa-lg mr-2" aria-hidden="true"></i> Google+
                            </a>
                        </div>

                        <br>

                        <h5 class="text-capitalize head-title mt-0 mb-4">All Comment</h5>
                        <ul class="comment-list">
                            <li>
                                <div class="card-author">
                                    <img src="assets/images/pictures/people-1.png" class="img-fluid img-profile img-rounded mr-4" alt="">
                                    <div class="info-profile">
                                        <p class="title title-font-500 mb-1">Sebastian Ferguson</p>
                                        <p class="text-gold mb-2">20 Juli 2018</p>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra, mi id rhoncus ultricies, magna eros ultricies leo, dictum aliquam lorem nisl vel sapien. Pellentesque lobortis mi id pellentesque 
                                        </p>
                                    </div>
                                </div>

                                <ul class="sub-comment">
                                    <li>
                                        <div class="card-author">
                                            <img src="assets/images/pictures/people-1.png" class="img-fluid img-profile img-rounded mr-4" alt="">
                                            <div class="info-profile">
                                                <p class="title title-font-500 mb-1">Sebastian Ferguson</p>
                                                <p class="text-gold mb-2">20 Juli 2018</p>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra, mi id rhoncus ultricies, magna eros ultricies leo, dictum aliquam lorem nisl vel sapien. Pellentesque lobortis mi id pellentesque 
                                                </p>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="card-author">
                                            <img src="assets/images/pictures/people-1.png" class="img-fluid img-profile img-rounded mr-4" alt="">
                                            <div class="info-profile">
                                                <p class="title title-font-500 mb-1">Sebastian Ferguson</p>
                                                <p class="text-gold mb-2">20 Juli 2018</p>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra, mi id rhoncus ultricies, magna eros ultricies leo, dictum aliquam lorem nisl vel sapien. Pellentesque lobortis mi id pellentesque 
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        <br>

                        <div class="row">
                            <div class="col-12 col-md-10 mb-4">
                                <h5 class="text-capitalize head-title mt-0 mb-2">Leave a Reply</h5>
                                <p class="head-sub-desc mb-2">Your email address will not be published</p>

                                <br>

                                <div class="form-group">
                                    <label for="">Fullname</label>
                                    <input type="text" name="" id="" class="form-control" placeholder="Your Name" aria-describedby="helpId">
                                </div>

                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="" id="" class="form-control" placeholder="example@mail.com" aria-describedby="helpId">
                                </div>

                                <div class="form-group">
                                    <label for="">Subject</label>
                                    <input type="text" name="" id="" class="form-control" placeholder="Subject" aria-describedby="helpId">
                                </div>

                                <div class="form-group">
                                    <label for="">Messege</label>
                                    <textarea class="form-control" rows="5" placeholder="Your Messege"></textarea>
                                </div>

                                <br>

                                <button type="submit" class="btn btn-primary btn-lg">Send Comment</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="form-group has-icon">
                    <h5 class="text-capitalize head-title mt-0 mb-3">Search</h5>
                    <span class="fa fa-search form-control-icon"></span>
                    <input type="text" name="" id="" class="form-control" placeholder="Type keyword">
                </div>

                <hr class="mt-4 mb-4">

                <div>
                    <h5 class="text-capitalize head-title mt-0 mb-3">Recent Pos</h5>
                    <div class="card-author mb-4">
                        <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                        <div class="info-profile">
                            <p class="title">Matching Jewellery Sets with your Outwear</p>
                            <div class="d-flex align-items-center">
                                <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                <p class="desc text-uppercase">August 16, 2018</p>
                            </div>
                        </div>
                    </div>

                    <div class="card-author mb-4">
                        <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                        <div class="info-profile">
                            <p class="title">Matching Jewellery Sets with your Outwear</p>
                            <div class="d-flex align-items-center">
                                <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                <p class="desc text-uppercase">August 16, 2018</p>
                            </div>
                        </div>
                    </div>

                    <div class="card-author mb-4">
                        <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                        <div class="info-profile">
                            <p class="title">Matching Jewellery Sets with your Outwear</p>
                            <div class="d-flex align-items-center">
                                <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                <p class="desc text-uppercase">August 16, 2018</p>
                            </div>
                        </div>
                    </div>

                    <div class="card-author mb-4">
                        <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                        <div class="info-profile">
                            <p class="title">Matching Jewellery Sets with your Outwear</p>
                            <div class="d-flex align-items-center">
                                <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                <p class="desc text-uppercase">August 16, 2018</p>
                            </div>
                        </div>
                    </div>

                    <div class="card-author mb-4">
                        <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                        <div class="info-profile">
                            <p class="title">Matching Jewellery Sets with your Outwear</p>
                            <div class="d-flex align-items-center">
                                <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                <p class="desc text-uppercase">August 16, 2018</p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="mt-4 mb-4">

                <div>
                    <h5 class="text-capitalize head-title mt-0 mb-3">Category</h5>
                    <ul class="category">
                        <li class="active">
                            <a href="#">
                                <p>All</p>
                                <p>(75)</p>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <p>Event</p>
                                <p>(75)</p>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <p>Pride</p>
                                <p>(75)</p>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <p>Promo</p>
                                <p>(75)</p>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <p>Dinar Dirham</p>
                                <p>(75)</p>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <p>Education</p>
                                <p>(75)</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>