<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/sub-header.png">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Our Product</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Our Product</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div id="ProductCategorySection" class="col-12 col-lg-4 mb-4">
                    <h5 class="text-capitalize head-title mt-0 mb-3">Search</h5>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Type Keyword">
                        <div class="input-group-append">
                            <button type="button" class="btn">
                                <i class="fa fa-search text-secondary"></i>
                            </button>
                        </div>
                    </div>

                    <hr class="mt-4 mb-4">

                    <div>
                        <h5 class="text-capitalize head-title mt-0 mb-3">Product Categories</h5>

                        <ul class="nav category tab-vertical flex-column" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="active" id="VTP_All" data-toggle="tab" href="#VTP_All_Content" role="tab" aria-controls="VTP_All_Content" aria-selected="true">
                                    <p>All Category</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a id="VTP_IndonesiaBatikSeries" data-toggle="tab" href="#VTP_IndonesiaBatikSeries_Content" role="tab" aria-controls="VTP_IndonesiaBatikSeries_Content" aria-selected="true">
                                    <p>Indonesia Batik Series</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a id="VTP_PureSilver" data-toggle="tab" href="#VTP_PureSilver_Content" role="tab" aria-controls="VTP_PureSilver_Content" aria-selected="false">
                                    <p>Pure Silver 99.5%</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a id="VTP_DinarDirham" data-toggle="tab" href="#VTP_DinarDirham_Content" role="tab" aria-controls="VTP_DinarDirham_Content" aria-selected="false">
                                    <p>Dinar and Dirham Coins</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a id="VTP_IndustrialProduct" data-toggle="tab" href="#VTP_IndustrialProduct_Content" role="tab" aria-controls="VTP_IndustrialProduct_Content" aria-selected="false">
                                    <p>Industrial Product</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a id="VTP_GiftSeries" data-toggle="tab" href="#VTP_GiftSeries_Content" role="tab" aria-controls="VTP_GiftSeries_Content" aria-selected="false">
                                    <p>Gift Series</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a id="VTP_ClassicProduct" data-toggle="tab" href="#VTP_ClassicProduct_Content" role="tab" aria-controls="VTP_ClassicProduct_Content" aria-selected="false">
                                    <p>Classic Product</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="ProductListSection" class="col-12 col-lg-8 mb-4">
                    <div class="row">
                        <div id="ProductContentSection" class="col-12">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="VTP_All_Content" role="tabpanel" aria-labelledby="VTP_All">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Golden Metal</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Golden Metal</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Golden Metal</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="VTP_IndonesiaBatikSeries_Content" role="tabpanel" aria-labelledby="VTP_IndonesiaBatikSeries">
                                    <p class="font-500 text-primary">Indonesia Batik Series</p>
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Golden Metal</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Golden Metal</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Golden Metal</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                                            <div class="card card-product">
                                                <div class="block-over-img img-lg">
                                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                                                </div>
                                                <div class="card-body">
                                                    <div class="content-list">
                                                        <p class="card-text text-secondary">Metal</p>
                                                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                                                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                                                    </div>
                                                    <hr>
                                                    <p class="card-text text-default text-center mb-0">
                                                        <a href="product-detail.php" class="text-default" tabindex="0">
                                                            <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                                            Detail
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="VTP_PureSilver_Content" role="tabpanel" aria-labelledby="VTP_PureSilver">
                                    ...
                                </div>

                                <div class="tab-pane fade" id="VTP_DinarDirham_Content" role="tabpanel" aria-labelledby="VTP_DinarDirham">
                                    ...
                                </div>

                                <div class="tab-pane fade" id="VTP_IndustrialProduct_Content" role="tabpanel" aria-labelledby="VTP_IndustrialProduct">
                                    ...
                                </div>

                                <div class="tab-pane fade" id="VTP_GiftSeries_Content" role="tabpanel" aria-labelledby="VTP_GiftSeries">
                                    ...
                                </div>

                                <div class="tab-pane fade" id="VTP_ClassicProduct_Content" role="tabpanel" aria-labelledby="VTP_ClassicProduct">
                                </div>
                            </div>
                        </div>

                        <br>
                        
                        <div id="ProductPaginationSection" class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center align-items-center w-100">
                                    <li class="page-item prev disabled">
                                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                                    <li class="page-item"><a class="page-link" href="#">9</a></li>
                                    <li class="page-item next">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div id="ProductFilterSection" class="col-12">
                            <div class="product-line-filter">
                                <p class="result mb-0">Showing all 5 results</p>
                                <div class="form-group sort mb-0">
                                    <select name="" id="" class="form-control w-auto filter-select-option">
                                        <option value="">Default Sorting</option>
                                        <option value="">Newest</option>
                                        <option value="">Price: Low to High</option>
                                        <option value="">Price: High to Low</option>
                                    </select>
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>