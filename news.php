<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/sub-header.png">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">News</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">News</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 mb-4">
                    <div class="row">
                        <div class="col-12 col-md-6 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">Event</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">Antam's 50 years of work</h5>
                                    <p class="card-text text-default">04 Juni 2019</p>
                                    <p class="card-text font-300">
                                        Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
                                    </p>
                                    <br>
                                    <a href="news-detail.php" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">Event</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">Antam's 50 years of work</h5>
                                    <p class="card-text text-default">04 Juni 2019</p>
                                    <p class="card-text font-300">
                                        Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
                                    </p>
                                    <br>
                                    <a href="news-detail.php" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">Event</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">Antam's 50 years of work</h5>
                                    <p class="card-text text-default">04 Juni 2019</p>
                                    <p class="card-text font-300">
                                        Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
                                    </p>
                                    <br>
                                    <a href="news-detail.php" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 mb-4">
                            <div class="card">
                                <div class="block-over-img img-lg">
                                    <img class="card-img-top img-fluid w-100" src="assets/images/pictures/50-years.png">
                                    <div class="top-left">
                                        <span class="badge badge-secondary">Event</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="text-gold font-title mb-2">Antam's 50 years of work</h5>
                                    <p class="card-text text-default">04 Juni 2019</p>
                                    <p class="card-text font-300">
                                        Lorem ipsum dolor sita met qonqueror ipsum dolor sita met qonqueror
                                    </p>
                                    <br>
                                    <a href="news-detail.php" class="btn btn-primary" tabindex="0">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center align-items-center w-100">
                            <li class="page-item prev disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">...</a></li>
                            <li class="page-item"><a class="page-link" href="#">9</a></li>
                            <li class="page-item next">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="col-12 col-lg-4 mb-4">
                    <div class="form-group has-icon">
                        <h5 class="text-capitalize head-title mt-0 mb-3">Search</h5>
                        <span class="fa fa-search form-control-icon"></span>
                        <input type="text" name="" id="" class="form-control" placeholder="Type keyword">
                    </div>

                    <hr class="mt-4 mb-4">

                    <div>
                        <h5 class="text-capitalize head-title mt-0 mb-3">Recent Pos</h5>
                        <div class="card-author mb-4">
                            <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                            <div class="info-profile">
                                <p class="title">Matching Jewellery Sets with your Outwear</p>
                                <div class="d-flex align-items-center">
                                    <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                    <p class="desc text-uppercase">August 16, 2018</p>
                                </div>
                            </div>
                        </div>

                        <div class="card-author mb-4">
                            <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                            <div class="info-profile">
                                <p class="title">Matching Jewellery Sets with your Outwear</p>
                                <div class="d-flex align-items-center">
                                    <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                    <p class="desc text-uppercase">August 16, 2018</p>
                                </div>
                            </div>
                        </div>

                        <div class="card-author mb-4">
                            <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                            <div class="info-profile">
                                <p class="title">Matching Jewellery Sets with your Outwear</p>
                                <div class="d-flex align-items-center">
                                    <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                    <p class="desc text-uppercase">August 16, 2018</p>
                                </div>
                            </div>
                        </div>

                        <div class="card-author mb-4">
                            <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                            <div class="info-profile">
                                <p class="title">Matching Jewellery Sets with your Outwear</p>
                                <div class="d-flex align-items-center">
                                    <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                    <p class="desc text-uppercase">August 16, 2018</p>
                                </div>
                            </div>
                        </div>

                        <div class="card-author mb-4">
                            <img src="assets/images/pictures/antam.png" class="img-fluid img-profile" alt="">
                            <div class="info-profile">
                                <p class="title">Matching Jewellery Sets with your Outwear</p>
                                <div class="d-flex align-items-center">
                                    <p class="desc mr-3"><a href="#" class="text-red text-uppercase font-500">Event</a></p>
                                    <p class="desc text-uppercase">August 16, 2018</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="mt-4 mb-4">

                    <div>
                        <h5 class="text-capitalize head-title mt-0 mb-3">Category</h5>
                        <ul class="category">
                            <li class="active">
                                <a href="#">
                                    <p>All</p>
                                    <span class="circle-count">75</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <p>Event</p>
                                    <span class="circle-count">75</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <p>Pride</p>
                                    <span class="circle-count">75</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <p>Promo</p>
                                    <span class="circle-count">75</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <p>Dinar Dirham</p>
                                    <span class="circle-count">75</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <p>Education</p>
                                    <span class="circle-count">75</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>