<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/sub-header.png">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Contact Us</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
        <div class="container">
            <h3 class="text-capitalize head-title">Let Us Know What You Have In Mind</h3>
            <p class="head-sub-desc">Visit our shop to see amazing creations from our designers</p>

            <div class="row m-t-70">
                <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-map-marker fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Store Location:</div>
                                    <p class="font-300 mb-0">
                                        Jl. Tubagus Ismail XV No. 14A, Sekeloa
                                        Coblong, Bandung 40133
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-phone fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Call Number:</div>
                                    <p class="font-300 mb-0">
                                        0822 3333 4545 <br>
                                        0813 8854 2222
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-envelope fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Email</div>
                                    <p class="font-300 mb-0">
                                        info@royalraffles.com <br>
                                        support@royalraffles.com
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-3 mb-4">
                    <div class="card-body p-0">
                        <div class="card-center-absolute">
                            <div class="d-flex">
                                <div class="mr-3">
                                    <i class="fa fa-clock-o fa-2x text-gold" aria-hidden="true"></i>
                                </div>

                                <div>
                                    <div class="card-title text-default mb-2">Working Hours</div>
                                    <p class="font-300 mb-0">
                                        Weekday 10.00am - 5.00pm. <br>
                                        Weekend 11.00 - 4.00pm.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container mb-4">
            <div class="row">
                <div class="col-12 col-md-6 mb-4">
                    <!-- <div class="mapouter">
                        <div class="mapouter">
                            <div class="gmap_canvas">
                                <iframe width="525" height="500" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q=Jl.%20tubagus%20ismail%20xv%20no%2014a&t=&z=19&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            </div>
                        </div>
                    </div> -->
                    <div id="map-contact" class="h-100"></div>
                </div>

                <div class="col-12 col-md-6 mb-4">
                    <h5 class="text-capitalize head-title mt-0 mb-2">Contact</h5>
                    <p class="head-sub-desc mb-2">Your email address will not be published</p>

                    <br>

                    <div class="form-group">
                        <label for="">Fullname</label>
                        <input type="text" name="" id="" class="form-control" placeholder="Your Name"
                            aria-describedby="helpId">
                    </div>

                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" name="" id="" class="form-control" placeholder="example@mail.com"
                            aria-describedby="helpId">
                    </div>

                    <div class="form-group">
                        <label for="">Subject</label>
                        <input type="text" name="" id="" class="form-control" placeholder="Subject"
                            aria-describedby="helpId">
                    </div>

                    <div class="form-group">
                        <label for="">Messege</label>
                        <textarea class="form-control" rows="5" placeholder="Your Messege"></textarea>
                    </div>

                    <br>

                    <button type="submit" class="btn btn-primary">Send Message</button>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>