<?php include("component/_head.php"); ?>
<?php include("component/_topbar.php"); ?>
<?php include("component/_navbar.php"); ?>

<div class="content-box content-home">
    <section class="sub-header pb-0">
        <div class="block-over-img card-image-only">
            <img class="img-bg img-fluid w-100" src="assets/images/pictures/sub-header.png">

            <div class="center-left w-100">
                <div class="text-block">
                    <h2 class="text-capitalize head-title text-white font-600 mb-2">Our Product</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="our-product.php">Our Product</a></li>
                            <li><a href="#">Diamond Ankle Bracelet 14K Gold 9</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank gray-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                <a href="assets/images/pictures/anting-1.png">
                                    <img src="assets/images/pictures/anting-1.png" alt="" width="640" height="360" />
                                </a>
                            </div>

                            <ul class="thumbnails">
                                <li>
                                    <a href="assets/images/pictures/anting-1.png" data-standard="assets/images/pictures/anting-1.png">
                                        <img src="assets/images/pictures/anting-1.png" alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="assets/images/pictures/cincin-1.png" data-standard="assets/images/pictures/cincin-1.png">
                                        <img src="assets/images/pictures/cincin-1.png" alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="assets/images/pictures/kalung-1.png" data-standard="assets/images/pictures/kalung-1.png">
                                        <img src="assets/images/pictures/kalung-1.png" alt="" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-7">
                    <div class="card-body">
                        <p class="card-text text-secondary mb-1">Metal</p>
                        <h2 class="text-default mb-4">Diamond Ankle Bracelet 14K Gold 9</h2>
                        <p class="card-text font-300">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra, mi id rhoncus ultricies, magna eros ultricies leo, dictum aliquam lorem nisl vel sapien. Pellentesque lobortis mi id pellentesque bibendum.
                        </p>
                        <br>
                        <h6 class="card-text font-desc text-gold font-500">Price</h6>
                        <h3 class="card-text font-desc text-default font-500">Rp 7.250.000</h3>
                        <div class="block-rules">
                            <div class="rule-icon"><i class="fa fa-info" aria-hidden="true"></i></div>
                            <div class="rule-text">
                                <div class="title">PPh 22, Pajak Penghasilan Pasal 22 atas emas batangan </div>
                                <p>Sesuai dengan PMK No 34/PMK.10/2017, pembelian emas batangan dikenakan PPh 22 sebesar 0,45% (untuk pemegang NPWP dan 0,9% untuk non NPWP). Setiap pembelian emas batangan disertai dengan bukti potong PPh 22.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-blank">
		<div class="container">
			<div class="row justify-content-between">
                <div class="col-12 col-lg-7">
                    <h3 class="text-capitalize head-title mb-4">Product Info</h3>
                    <p class="font-300">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Provident deserunt hic expedita aliquam error ipsam maxime reiciendis, non veniam corrupti fuga nam in, veritatis odio eveniet nisi quos unde repudiandae!</p>
                    <p class="font-300">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Provident deserunt hic expedita aliquam error ipsam maxime reiciendis, non veniam corrupti fuga nam in, veritatis odio eveniet nisi quos unde repudiandae!</p>
                </div>

                <div class="col-12 col-lg-4">
                    <h3 class="text-capitalize head-title mb-4">Spesification</h3>
                    <div class="mb-3">
                        <p class="font-500 mb-1">Weight</p>
                        <p class="font-300 mb-1">10 gram, 20 gram</p>
                    </div>

                    <div class="mb-3">
                        <p class="font-500 mb-1">Purity</p>
                        <p class="font-300 mb-1">99.99%</p>
                    </div>

                    <div class="mb-3">
                        <p class="font-500 mb-1">Thick</p>
                        <p class="font-300 mb-1">1.0 mm, 1.1 mm</p>
                    </div>

                    <div class="mb-3">
                        <p class="font-500 mb-1">Dimension</p>
                        <p class="font-300 mb-1">18 x 30 mm, 24 x 40 mm</p>
                    </div>
                </div>
            </div>
		</div>
    </section>
    
    <section class="content-blank gray-section pb-6">
		<div class="container">
            <h3 class="text-capitalize head-title">Related Product</h3>
            <p class="head-sub-desc">Lorem ipsum dolor sita met qonqueror</p>

			<div class="slick_related-product slick-custom mt-4">
                <div class="card card-slick-custom">
                    <div class="block-over-img img-lg">
                        <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                    </div>
                    <div class="card-body">
                        <p class="card-text text-secondary">Metal</p>
                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                        <hr>
                        <p class="card-text text-default text-center">
                            <a href="product-detail.php" class="text-default">
                                <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                Detail
                            </a>
                        </p>
                    </div>
                </div>

                <div class="card card-slick-custom">
                    <div class="block-over-img img-lg">
                        <img class="card-img-top img-fluid w-100" src="assets/images/pictures/kalung-1.png">
                    </div>
                    <div class="card-body">
                        <p class="card-text text-secondary">Metal</p>
                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                        <hr>
                        <p class="card-text text-default text-center">
                            <a href="product-detail.php" class="text-default">
                                <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                Detail
                            </a>
                        </p>
                    </div>
                </div>

                <div class="card card-slick-custom">
                    <div class="block-over-img img-lg">
                        <img class="card-img-top img-fluid w-100" src="assets/images/pictures/cincin-1.png">
                    </div>
                    <div class="card-body">
                        <p class="card-text text-secondary">Metal</p>
                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                        <hr>
                        <p class="card-text text-default text-center">
                            <a href="product-detail.php" class="text-default">
                                <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                Detail
                            </a>
                        </p>
                    </div>
                </div>

                <div class="card card-slick-custom">
                    <div class="block-over-img img-lg">
                        <img class="card-img-top img-fluid w-100" src="assets/images/pictures/anting-1.png">
                    </div>
                    <div class="card-body">
                        <p class="card-text text-secondary">Metal</p>
                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                        <hr>
                        <p class="card-text text-default text-center">
                            <a href="product-detail.php" class="text-default">
                                <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                Detail
                            </a>
                        </p>
                    </div>
                </div>

                <div class="card card-slick-custom">
                    <div class="block-over-img img-lg">
                        <img class="card-img-top img-fluid w-100" src="assets/images/pictures/cincin-1.png">
                    </div>
                    <div class="card-body">
                        <p class="card-text text-secondary">Metal</p>
                        <p class="text-default mb-2">Diamond Ankle Bracelet 14K Gold 9</p>
                        <p class="card-text text-gold font-300">Rp 2.000.000</p>
                        <hr>
                        <p class="card-text text-default text-center">
                            <a href="product-detail.php" class="text-default">
                                <i class="fa fa-eye mr-1 text-secondary" aria-hidden="true"></i> View
                                Detail
                            </a>
                        </p>
                    </div>
                </div>
            </div>
		</div>
	</section>
</div>

<?php include("component/_footer.php"); ?>
<?php include("component/_modal.php"); ?>
<?php include("component/_foot.php"); ?>